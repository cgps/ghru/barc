import re
import yaml
import os

from functions.utilities import read_yaml_files
from functions.rule_functions import match_all_determinants, match_suppressor

# read in rules
rules_directory = 'amr-rules'
rules = read_yaml_files(rules_directory)
print(rules)

# read in determinant groups
determinant_groups_directory = 'amr-determinant-groups'
determinant_groups = read_yaml_files(determinant_groups_directory)
print(determinant_groups)

suppressor_groups_directory = 'amr-suppressor-groups'
suppressor_groups = read_yaml_files(suppressor_groups_directory)
print(suppressor_groups)


def test_rules(rules, isolate_determinants):
  matching_rules = []
  any_match = False
  # check each rule
  for rule in rules:
    # for a single rule each determinant group needs to have a match
    match_all_determinant_groups = True
    # check each determinant group
    for determinant_group in rule['determinant_groups']:
      determinant_group_match = match_all_determinants(determinant_group, determinant_groups, isolate_determinants)
      # check suppressors for determinant group
      suppressor_match = match_suppressor(determinant_group, suppressor_groups, isolate_determinants)
      # if the determinant group is not a match or a suppressor overrides the match, at least one group is a non-match
      # and so all determinant groups for a rule do not have a match so the rule has failed
      if not determinant_group_match or suppressor_match:
        match_all_determinant_groups = False
        
    if match_all_determinant_groups:  
      matching_rules.append(",".join([group['name'] for group in rule['determinant_groups']]))
      any_match = True
  return(any_match, ";".join(matching_rules))    





isolate_1_determinants = ['NDM-1']
isolate_2_determinants = ['CTX-M-15', 'ompk36-mut1']
isolate_3_determinants = ['SHV-12']
isolate_4_determinants = ['SHV-12', 'ompk36-loss']
isolate_5_determinants = ['SHV-12', 'ompk36-loss', 'ompk36-suppressor-mut1']
carbapenem_rules = rules['carbapenems']


match, matching_rules = test_rules(carbapenem_rules, isolate_1_determinants)
print(match, matching_rules)

match, matching_rules = test_rules(carbapenem_rules, isolate_2_determinants)
print(match, matching_rules)

match, matching_rules = test_rules(carbapenem_rules, isolate_3_determinants)
print(match, matching_rules)

match, matching_rules = test_rules(carbapenem_rules, isolate_4_determinants)
print(match, matching_rules)

match, matching_rules = test_rules(carbapenem_rules, isolate_5_determinants)
print(match, matching_rules)