import re
def match_all_determinants(determinant_group, determinant_groups, isolate_determinants):
    determinant_patterns = determinant_groups[determinant_group['name']]
    # check all possible determinants for a group e.g NDM-[0-9]+, KPC-[0-9+]
    # if any match it's a match for the group
    determinant_group_match = False
    for determinant_pattern_string in determinant_patterns:
      determinant_pattern = re.compile(determinant_pattern_string)
      if any(determinant_pattern.match(determinant) for determinant in isolate_determinants):
        determinant_group_match = True
    return determinant_group_match

def match_suppressor(determinant_group, suppressor_groups, isolate_determinants):
    suppressor_match = False
    if determinant_group['suppressors']:
      for suppressor_group in determinant_group['suppressors']:
        suppressor_patterns = suppressor_groups[suppressor_group]
        suppressor_group_match = False
        for suppressor_pattern_string in suppressor_patterns:
          suppressor_pattern = re.compile(suppressor_pattern_string)
          if any(suppressor_pattern.match(determinant) for determinant in isolate_determinants):
            suppressor_group_match = True
        if suppressor_group_match:
          suppressor_match = True
    return suppressor_match