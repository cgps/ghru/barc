import os
import yaml
def read_yaml_files(directory):
  files = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]

  data_collection = {}
  for file in files:
    file_suffix, ext = os.path.splitext(file)
    with open(os.path.join(directory, file)) as fh:
        data = yaml.load(fh, Loader=yaml.FullLoader)
        data_collection[file_suffix] = data
  return data_collection